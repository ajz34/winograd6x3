# Winograd F(6,3) 简易实现

编译需要在支持 AVX512 的设备上进行。cmake 该项目即可。

为了与 Intel oneAPI DNNL 进行效率比较，需要在 CMakeLists.txt 文件中更改 `DNNLROOT` 的路径。如果只是希望编译本工作的 winograd_f6x3 程序，请在 CMakeLists.txt 文件中手动将与 DNNL 有关的部分删去。

编译后，对于 VGG16 网络，运行 `run_winograd_f6x3 vgg16.conf` 可以进行效率测评。

该项目的文档请参考 [ajz34 文档](https://ajz34.readthedocs.io/zh_CN/latest/ML_Notes/winograd6x3/cnn_winograd_l2.html)。

该项目是基于 Ubiquant Challenge 量化新星挑战赛|并行程序设计竞赛的**初赛工作**。赛题 baseline 请参考 [gitee: benjie-miao/winograd-baseline](https://gitee.com/benjie-miao/winograd-baseline)。决赛获得冠军队伍的工作，请参考 [github: Robslhc/ubiquant-winograd](http://github.com/Robslhc/ubiquant-winograd)。
